#include <stdio.h>
#include <string.h>
int main()
{
	int k, i = 0;
	char buf[256];
	printf("Enter a line, please: \n");
	fgets(buf, 256, stdin);
	k = (strlen(buf) - 1) % 3;
	for (i = 0; i < strlen(buf); ++i)
	{
		printf("%c", buf[i]);
		if ((i + 1) % 3 == k)
			printf(" ");
	}
	return 0;
}
